/**
 * @description 自动import导入所有 api 模块
 *
 * require.context(directory,useSubdirectories,regExp)
 *   directory:表示检索的目录
 *   useSubdirectories：表示是否检索子文件夹
 *   regExp:匹配文件的正则表达式,一般是文件名
 */

const files = require.context('./model', false, /\.js$/)
const modules = {}
files.keys().forEach((key) => {
	modules[key.replace(/(\.\/|\.js)/g, '')] = files(key).default
})

export default modules
