import config from "@/config"
import http from "@/utils/request"

export default {
	dragonList: {
		url: `${config.API_URL}/admin/dragon/list`,
		name: "优惠套餐列表",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	dragonAdd: {
		url: `${config.API_URL}/admin/dragon/add`,
		name: "优惠套餐新增初始化",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	dragonEdit: {
		url: `${config.API_URL}/admin/dragon/edit`,
		name: "优惠套餐编辑初始化",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	dragonSave: {
		url: `${config.API_URL}/admin/dragon/save`,
		name: "优惠套餐提交保存",
		post: async function (params) {
			return await http.post(this.url, params);
		}
	},

	dragonUpdate: {
		url: `${config.API_URL}/admin/dragon/update`,
		name: "优惠套餐编辑保存",
		post: async function (params) {
			return await http.post(this.url, params);
		}
	},


	dragonDel: {
		url: `${config.API_URL}/admin/dragon/delete`,
		name: "优惠套餐删除",
		post: async function (params) {
			return await http.post(this.url, params);
		}
	},


	promotionList: {
		url: `${config.API_URL}/admin/promotion/list`,
		name: "秒杀/单品活动列表",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	promotionAdd: {
		url: `${config.API_URL}/admin/promotion/add`,
		name: "秒杀/单品活动新增初始化",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},


	promotionEdit: {
		url: `${config.API_URL}/admin/promotion/edit`,
		name: "秒杀/单品活动编辑初始化",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	promotionSave: {
		url: `${config.API_URL}/admin/promotion/save`,
		name: "秒杀/单品活动新增提交保存",
		post: async function (params) {
			return await http.post(this.url, params);
		}
	},



	promotionUpdate: {
		url: `${config.API_URL}/admin/promotion/update`,
		name: "秒杀/单品活动编辑保存",
		post: async function (params) {
			return await http.post(this.url, params);
		}
	},

	promotionDel: {
		url: `${config.API_URL}/admin/promotion/delete`,
		name: "秒杀/单品活动删除",
		post: async function (params) {
			return await http.post(this.url, params);
		}
	},

	// 全场活动
	listActivity: {
		url: `${config.API_URL}/admin/activity/list`,
		name: "全场活动列表",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	activityAdd: {
		url: `${config.API_URL}/admin/activity/add`,
		name: "全场活动新增初始化",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},


	activityEdit: {
		url: `${config.API_URL}/admin/activity/edit`,
		name: "全场活动编辑初始化",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	activitySave: {
		url: `${config.API_URL}/admin/activity/save`,
		name: "全场活动新增提交保存",
		post: async function (params) {
			return await http.post(this.url, params);
		}
	},



	activityUpdate: {
		url: `${config.API_URL}/admin/activity/update`,
		name: "全场活动编辑保存",
		post: async function (params) {
			return await http.post(this.url, params);
		}
	},

	activityDel: {
		url: `${config.API_URL}/admin/activity/delete`,
		name: "全场活动杀删除",
		post: async function (params) {
			return await http.post(this.url, params);
		}
	},

	// 优惠券
	couponList: {
		url: `${config.API_URL}/admin/coupon/list`,
		name: "优惠券列表",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	couponAdd: {
		url: `${config.API_URL}/admin/coupon/add`,
		name: "优惠券新增初始化",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},


	couponEdit: {
		url: `${config.API_URL}/admin/coupon/edit`,
		name: "优惠券编辑初始化",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	couponSave: {
		url: `${config.API_URL}/admin/coupon/save`,
		name: "优惠券新增提交保存",
		post: async function (params) {
			return await http.post(this.url, params);
		}
	},



	couponUpdate: {
		url: `${config.API_URL}/admin/coupon/update`,
		name: "优惠券编辑保存",
		post: async function (params) {
			return await http.post(this.url, params);
		}
	},

	couponDel: {
		url: `${config.API_URL}/admin/coupon/delete`,
		name: "优惠券删除",
		post: async function (params) {
			return await http.post(this.url, params);
		}
	},

	couponSend: {
		url: `${config.API_URL}/admin/coupon/send`,
		name: "优惠券投放",
		post: async function (params) {
			return await http.post(this.url, params);
		}
	},

	couponDetail: {
		url: `${config.API_URL}/admin/couponCode/coupon_summary_detail`,
		name: "优惠券明细",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},














}
