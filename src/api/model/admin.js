import config from "@/config"
import http from "@/utils/request"

export default {
	// 员工列表
	admin: {
		list: {
			url: `${config.API_URL}/admin/admin/admin_list`,
			name: "店铺列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
	}
}
