import config from "@/config"
import http from "@/utils/request"

export default {

	dayReport: {
		url: `${config.API_URL}/admin/report/day_report_index`,
		name: "营业日报",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	orderSummary: {
		url: `${config.API_URL}/admin/report/order_summary`,
		name: "商品统计表",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},
	orderExport: {
		url: `${config.API_URL}/admin/report/order_export.jhtml`,
		name: "商品统计表导出",
		get: async function (data, config = {responseType: 'blob'}) {
			return await http.get(this.url, data, config);
		}
	},

	orderDetail: {
		url: `${config.API_URL}/admin/report/order_detail`,
		name: "订单明细",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},
	orderReport: {
		url: `${config.API_URL}/admin/report/order_detail_export.jhtml`,
		name: "订单明细导出",
		get: async function (data, config = {responseType: 'blob'}) {
			return await http.get(this.url, data, config);
		}
	},
	payment: {
		url: `${config.API_URL}/admin/report/payment_summary`,
		name: "收款统计",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	paymentReport: {
		url: `${config.API_URL}/admin/report/payment_export.jhtml`,
		name: "收款统计导出",
		get: async function (data, config = {responseType: 'blob'}) {
			return await http.get(this.url, data, config);
		}
	},

	paymentInfo: {
		url: `${config.API_URL}/admin/report/payment_detail`,
		name: "收款明细",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	paymentReportDetail: {
		url: `${config.API_URL}/admin/report/payment_detail_export.jhtml`,
		name: "收款明细导出",
		get: async function (data, config = {responseType: 'blob'}) {
			return await http.get(this.url, data, config);
		}
	},


	balanceSummary: {
		url: `${config.API_URL}/admin/report/balance_summary`,
		name: "余额统计",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	updateBalanceList: {
		url: `${config.API_URL}/admin/card/update_balance_list`,
		name: "余额统计查看明细",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	balanceReport: {
		url: `${config.API_URL}/admin/report/balance_export.jhtml`,
		name: "余额统计导出",
		get: async function (data, config = {responseType: 'blob'}) {
			return await http.get(this.url, data, config);
		}
	},


	balanceDetail: {
		url: `${config.API_URL}/admin/report/balance_detail`,
		name: "余额明细",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	balanceReportDetail: {
		url: `${config.API_URL}/admin/report/balance_detail_export.jhtml`,
		name: "余额明细导出",
		get: async function (data, config = {responseType: 'blob'}) {
			return await http.get(this.url, data, config);
		}
	},

	pointSummary: {
		url: `${config.API_URL}/admin/report/business_list.jhtml`,
		name: "金币统计",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	pointSummaryExport: {
		url: `${config.API_URL}/admin/report/business_export.jhtml`,
		name: "金币统计导出",
		get: async function (data, config = {responseType: 'blob'}) {
			return await http.get(this.url, data, config);
		}
	},

	pointList: {
		url: `${config.API_URL}/admin/card/pointList`,
		name: "金币明细",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},



	pointExport: {
		url: `${config.API_URL}/admin/card/pointExport.jhtml`,
		name: "金币明细导出",
		get: async function (data, config = {responseType: 'blob'}) {
			return await http.get(this.url, data, config);
		}
	},

	barrelSummary: {
		url: `${config.API_URL}/admin/report/barrel_summary`,
		name: "空桶统计",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	barrelExport: {
		url: `${config.API_URL}/admin/report/barrel_export.jhtml`,
		name: "空桶统计导出",
		get: async function (data, config = {responseType: 'blob'}) {
			return await http.get(this.url, data, config);
		}
	},


	shippingBarrelList: {
		url: `${config.API_URL}/admin/barrel/shippingBarrel_list`,
		name: "空桶统计送货记录",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	barrelLog: {
		url: `${config.API_URL}/admin/report/barrelLog_summary`,
		name: "押桶统计",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	barrelLogDetail: {
		url: `${config.API_URL}/admin/report/barrelLog_detail`,
		name: "押桶统计明细",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	waterSummary: {
		url: `${config.API_URL}/admin/report/water_summary`,
		name: "水票统计",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	waterDetail: {
		url: `${config.API_URL}/admin/report/water_detail`,
		name: "水票统计剩余明细",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	waterGlide: {
		url: `${config.API_URL}/admin/report/water_glide`,
		name: "水票统计其他明细",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	monthlySummary: {
		url: `${config.API_URL}/admin/report/monthly_summary`,
		name: "记账列表",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	monthlyExport: {
		url: `${config.API_URL}/admin/report/monthly_export.jhtml`,
		name: "记账列表导出",
		get: async function (data, config = {responseType: 'blob'}) {
			return await http.get(this.url, data, config);
		}
	},

	monthlyOrderExport: {
		url: `${config.API_URL}/admin/report/monthly_order_export.jhtml`,
		name: "记账列表导出",
		get: async function (data, config = {responseType: 'blob'}) {
			return await http.get(this.url, data, config);
		}
	},



	monthly: {
		url: `${config.API_URL}/admin/report/monthly`,
		name: "记账还款确认",
		post: async function (params) {
			return await http.post(this.url, params);
		}
	},

	monthlyOrder: {
		url: `${config.API_URL}/admin/report/monthly_order_summary`,
		name: "记账订单列表",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	monthlyMpayee: {
		url: `${config.API_URL}/admin/report/monthly_mpayee_summary`,
		name: "记账还款列表",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	empSummary: {
		url: `${config.API_URL}/admin/report/shipping_emp_summary`,
		name: "员工送货统计",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	empDetail: {
		url: `${config.API_URL}/admin/report/shipping_emp_summary_detail`,
		name: "员工送货统计",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},


	empExport: {
		url: `${config.API_URL}/admin/report/shipping_export.jhtml`,
		name: "员工送货统计导出",
		get: async function (data, config = {responseType: 'blob'}) {
			return await http.get(this.url, data, config);
		}
	},


	shippingDetail: {
		url: `${config.API_URL}/admin/report/shipping_detail`,
		name: "员工送货明细",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	shippingDetailExport: {
		url: `${config.API_URL}/admin/report/shipping_detail_export.jhtml`,
		name: "员工送货明细导出",
		get: async function (data, config = {responseType: 'blob'}) {
			return await http.get(this.url, data, config);
		}
	},

	promoterSummary: {
		url: `${config.API_URL}/admin/report/promoter_summary`,
		name: "员工推广统计",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	promoterExport: {
		url: `${config.API_URL}/admin/report/promoter_export.jhtml`,
		name: "员工推广统计导出",
		get: async function (data, config = {responseType: 'blob'}) {
			return await http.get(this.url, data, config);
		}
	},


	promoterCard: {
		url: `${config.API_URL}/admin/report/promoter_card_summary`,
		name: "推广会员列表",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	promoterDeposit: {
		url: `${config.API_URL}/admin/report/promoter_deposit`,
		name: "分润详情列表",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},















}
