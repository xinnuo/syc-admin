import config from "@/config"
import http from "@/utils/request"

export default {
	preLogin: {
		url: `${config.API_URL}/admin/login/index`,
		name: "登录初始",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},
	// 验证码
	captcha: `${config.API_URL}/admin/common/captcha.jhtml?timestamp=`,

	token: {
		url: `${config.API_URL}/admin/login/submit`,
		name: "登录",
		post: async function (data = {}) {
			return await http.post(this.url, data);
		}
	},
	change: {
		url: `${config.API_URL}/admin/admin/switch`,
		name: "切换店铺",
		post: async function (data = {}) {
			return await http.post(this.url, data);
		}
	},
	info: {
		url: `${config.API_URL}/admin/admin/attribute`,
		name: "个人信息",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},
	pwd: {
		url: `${config.API_URL}/admin/password/update`,
		name: "修改密码",
		post: async function (data = {}) {
			return await http.post(this.url, data);
		}
	},
	noticeInfo: {
		url: `${config.API_URL}/admin/article/notice`,
		name: "公告信息",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},
	notice: {
		url: `${config.API_URL}/admin/article/notice`,
		name: "下发公告",
		post: async function (data = {}) {
			return await http.post(this.url, data);
		}
	},
	logout: {
		url: `${config.API_URL}/admin/login/logout`,
		name: "退出登录",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	}
}
