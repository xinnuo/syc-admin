import config from "@/config"
import http from "@/utils/request"

export default {
	// 商品分类
	category: {
		list: {
			url: `${config.API_URL}/admin/productCategory/list`,
			name: "列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		info: {
			url: `${config.API_URL}/admin/productCategory/detail`,
			name: "详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		save: {
			url: `${config.API_URL}/admin/productCategory/save`,
			name: "新增",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		update: {
			url: `${config.API_URL}/admin/productCategory/update`,
			name: "更新",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delete: {
			url: `${config.API_URL}/admin/productCategory/delete`,
			name: "删除",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		}
	},

	// 空桶押金
	brand: {
		list: {
			url: `${config.API_URL}/admin/barrel/list`,
			name: "列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		save: {
			url: `${config.API_URL}/admin/barrel/save`,
			name: "添加",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delete: {
			url: `${config.API_URL}/admin/barrel/delete`,
			name: "删除",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		update: {
			url: `${config.API_URL}/admin/barrel/update`,
			name: "更新",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		}
	},

	// 商品档案
	product: {
		list: {
			url: `${config.API_URL}/admin/product/list`,
			name: "列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		info: {
			url: `${config.API_URL}/admin/product/detail`,
			name: "详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		detail: {
			url: `${config.API_URL}/admin/product/content_detail`,
			name: "产品详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		export: {
			url: `${config.API_URL}/admin/product/export`,
			name: "导出商品",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
		syncAll: {
			url: `${config.API_URL}/admin/product/import_all`,
			name: "同步店铺",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		save2: {
			url: `${config.API_URL}/admin/product/content_save`,
			name: "保存详情",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		market: {
			url: `${config.API_URL}/admin/product/isMarketable`,
			name: "上/下架",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		tag: {
			url: `${config.API_URL}/admin/tag/list`,
			name: "标签列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		supply: {
			url: `${config.API_URL}/admin/supplier/list`,
			name: "供应商",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		distribution: {
			url: `${config.API_URL}/admin/distribution/list`,
			name: "销售策略",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		freight: {
			url: `${config.API_URL}/admin/freight/list`,
			name: "运费模板",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		save: {
			url: `${config.API_URL}/admin/product/save`,
			name: "添加",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		sync: {
			url: `${config.API_URL}/admin/product/sync_all`,
			name: "报货同步",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delete: {
			url: `${config.API_URL}/admin/product/delete`,
			name: "删除",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		update: {
			url: `${config.API_URL}/admin/product/update`,
			name: "更新",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		}
	},

	// 水票管理
	ticket: {
		list: {
			url: `${config.API_URL}/admin/bomWater/list`,
			name: "列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		info: {
			url: `${config.API_URL}/admin/bomWater/detail`,
			name: "详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		save: {
			url: `${config.API_URL}/admin/bomWater/save`,
			name: "新增水票",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delete: {
			url: `${config.API_URL}/admin/bomWater/delete`,
			name: "删除",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		update: {
			url: `${config.API_URL}/admin/bomWater/update`,
			name: "更新",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		}
	},

	// 订单来源
	method: {
		list: {
			url: `${config.API_URL}/admin/orderMethod/list`,
			name: "列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		save: {
			url: `${config.API_URL}/admin/orderMethod/save`,
			name: "编辑",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delete: {
			url: `${config.API_URL}/admin/orderMethod/delete`,
			name: "编辑",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
	},

	// 图片上传
	upload: {
		url: `${config.API_URL}/file/upload`,
		name: "图片上传",
		post: async function (data, config = {}) {
			return await http.post(this.url, data, config);
		}
	},

	// 附件上传
	uploadFile: {
		url: `${config.API_URL}/file/upload`,
		name: "附件上传",
		post: async function (data, config = {}) {
			return await http.post(this.url, data, config);
		}
	}
}
