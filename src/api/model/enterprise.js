import config from "@/config"
import http from "@/utils/request"

export default {
	// 服务商
	enterprise: {
		list: {
			url: `${config.API_URL}/admin/enterprise/list`,
			name: "服务商列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		detail: {
			url: `${config.API_URL}/admin/enterprise/detail`,
			name: "服务商详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		station: {
			url: `${config.API_URL}/admin/enterprise/select_list`,
			name: "水站列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		driver: {
			url: `${config.API_URL}/admin/enterprise/driver_list`,
			name: "司机列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
	},

	// 服务商会员列表
	shop: {
		list: {
			url: `${config.API_URL}/admin/shop/list`,
			name: "店铺列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		info: {
			url: `${config.API_URL}/admin/shop/detail`,
			name: "店铺明细",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		addshop:{
			url:`${config.API_URL}/admin/shop/save`,
			name:"添加店铺",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		editshop:{
			url:`${config.API_URL}/admin/shop/update`,
			name:"编辑店铺",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		stmeituan:{
			url:`${config.API_URL}/admin/shop/meituan_product_save`,
			name:"设置美团",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delete:{
			url: `${config.API_URL}/admin/shop/delete`,
			name: "删除店铺",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		bindprin:{
			url: `${config.API_URL}/admin/shop/printer`,
			name: "绑定打印机",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		unbindprin:{
			url: `${config.API_URL}/admin/shop/removePrinter`,
			name: "解绑打印机",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		shopfencelist:{
			url: `${config.API_URL}/admin/geo_fence/list`,
			name: "地图围栏",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		delshopfence:{
			url: `${config.API_URL}/admin/geo_fence/delete`,
			name: "删除地图围栏",
			post: async function (data,config={}) {
				return await http.post(this.url, data,config);
			}
		},
		getfenceaddshop:{
			url: `${config.API_URL}/admin/geo_fence/shops`,
			name: "地图围栏门店列表",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		shopfenceaddinit:{
			url: `${config.API_URL}/admin/geo_fence/add`,
			name: "店铺地图围栏添加初始化",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		shopfenceeditinit:{
			url: `${config.API_URL}/admin/geo_fence/edit`,
			name: "店铺地图围栏编辑初始化",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},

		shopfenceviewinit:{
			url: `${config.API_URL}/admin/geo_fence/view`,
			name: "地图围栏初始化",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		shopfencesave:{
			url: `${config.API_URL}/admin/geo_fence/save`,
			name: "添加地图围栏",
			post: async function (data,config={}) {
				return await http.post(this.url, data,config);
			}
		},
		shopfenceupdate:{
			url: `${config.API_URL}/admin/geo_fence/update`,
			name: "修改地图围栏",
			post: async function (data,config={}) {
				return await http.post(this.url, data,config);
			}
		},
	},
	pickup: {
		getqrcode:{
			url: `${config.API_URL}/admin/shopDummy/shop_qrcode_index`,
			name: "二维码",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getqrcodeimg:{
			url: `${config.API_URL}/admin/shopDummy/shop_qrcode`,
			name: "二维码图片",
			get: async function (params, config = {responseType: 'blob'}) {
				return await http.get(this.url, params,config);
			}
		},
		list: {
			url: `${config.API_URL}/admin/shopDummy/list`,
			name: "站点列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		pickupgoodslist: {
			url: `${config.API_URL}/admin/shopDummy/product_list`,
			name: "自提点商品列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		importgoods: {
			url: `${config.API_URL}/admin/shopDummy/product_import`,
			name: "导入商品",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		addpikup:{
			url: `${config.API_URL}/admin/shopDummy/save`,
			name: "添加站点",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		updatepikup:{
			url: `${config.API_URL}/admin/shopDummy/update`,
			name: "编辑站点",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delpikup:{
			url: `${config.API_URL}/admin/shopDummy/delete`,
			name: "删除站点",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		tongji: {
			url: `${config.API_URL}/admin/shopDummy/product_report_list`,
			name: "统计",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		daochu: {
			url: `${config.API_URL}/admin/shopDummy/product_report_export`,
			name: "导出",
			get: async function (data, config = {responseType: 'blob'}) {
				return await http.get(this.url, data, config);
			}
		},
		editprice:{
			url: `${config.API_URL}/admin/shopDummy/edit_price`,
			name: "修改自提点商品价格",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		editstock:{
			url: `${config.API_URL}/admin/shopDummy/edit_stock`,
			name: "修改自提点商品库存",//type 0增加 1减少
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delgoods:{
			url: `${config.API_URL}/admin/shopDummy/delete_product`,
			name: "删除商品",//type 0增加 1减少
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		// goodslist: {
		// 	url: `${config.API_URL}/admin/shopDummy/product_report_list`,
		// 	name: "商品",
		// 	get: async function (params) {
		// 		return await http.get(this.url, params);
		// 	}
		// },

		ztgqrcode: {
			url: `${config.API_URL}/admin/shopDummy/shop_cabinet_qrcode_index`,
			name: "自提柜二维码",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		ztglist: {
			url: `${config.API_URL}/admin/shopDummy/cabinet_list`,
			name: "自提柜列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		addztg: {
			url: `${config.API_URL}/admin/shopDummy/cabinet_save`,
			name: "添加自提柜",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		editztg: {
			url: `${config.API_URL}/admin/shopDummy/cabinet_update`,
			name: "编辑自提柜",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delztg:{
			url: `${config.API_URL}/admin/shopDummy/cabinet_delete`,
			name: "删除自提柜",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		restartztg:{
			url: `${config.API_URL}/admin/shopDummy/cabinet_restart`,
			name: "重启自提柜",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		ztggoodslist: {
			url: `${config.API_URL}/admin/shopDummy/cabinet_cell_edit`,
			name: "自提商品柜列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		addgoods: {
			url: `${config.API_URL}/admin/shopDummy/cabinet_cell_save`,
			name: "自提商品柜列表",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},

	},
	emaster:{
		masterlist:{
			url:`${config.API_URL}/admin/admin/list`,
			name: "员工管理列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		czpassword:{
			url: `${config.API_URL}/admin/admin/reset`,
			name: "重置密码",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		addmaster:{
			url: `${config.API_URL}/admin/admin/save`,
			name: "添加员工",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		initmasteradd:{
			url: `${config.API_URL}/admin/admin/add`,
			name: "添加员工初始化",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		initmasteredit:{
			url: `${config.API_URL}/admin/admin/edit`,
			name: "修改员工初始化",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		editmaster:{
			url: `${config.API_URL}/admin/admin/update`,
			name: "修改员工",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		fenceindex:{
			url: `${config.API_URL}/admin/admin/fence_index`,
			name: "员工围栏",
			get: async function (data) {
				return await http.get(this.url, data);
			}

		},
		coordindex:{
			url: `${config.API_URL}/lbs/geoCode`,
			name: "坐标",
			get: async function (data) {
				return await http.get(this.url, data);
			}

		},
		fencesave:{
			url: `${config.API_URL}/admin/admin/fence_save`,
			name: "添加员工围栏",
			post: async function (data,config = {}) {
				return await http.post(this.url, data,config);
			}

		},
        fenceupdate:{
            url: `${config.API_URL}/admin/admin/fence_update`,
            name: "添加员工围栏",
            post: async function (data,config = {}) {
                return await http.post(this.url, data,config);
            }

        },
		fencedel:{
			url: `${config.API_URL}/admin/admin/fence_delete`,
			name: "删除员工围栏",
			post: async function (data,config = {}) {
				return await http.post(this.url, data,config);
			}

		},
	},
	eteam:{
		teamlist: {
			url: `${config.API_URL}/admin/card/promoteList`,
			name: "团队列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		carddetil: {
			url: `${config.API_URL}/admin/card/view`,
			name: "团队列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		teamexport: {
			url: `${config.API_URL}/admin/card/promoteExport`,
			name: "团队列表导出",
			get: async function (data, config = {responseType: 'blob'}) {
				return await http.get(this.url, data, config);
			}
		},
	},
	erole:{
		rolelist: {
			url: `${config.API_URL}/admin/role/list`,
			name: "角色列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		rolepermisson: {
			url: `${config.API_URL}/admin/power/list`,
			name: "角色权限",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		postpermisson: {
			url: `${config.API_URL}/admin/role/authorized`,
			name: "设置权限",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		postrole: {
			url: `${config.API_URL}/admin/role/save`,
			name: "添加角色",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		updatarole: {
			url: `${config.API_URL}/admin/role/update`,
			name: "编辑角色",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		delrole: {
			url: `${config.API_URL}/admin/role/delete`,
			name: "编辑角色",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
	},
	eservice:{
		guanlilist: {
			url: `${config.API_URL}/admin/enterpriseOp/enterprise_list`,
			name: "经营管理",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		servicelist: {
			url: `${config.API_URL}/admin/enterpriseOp/list`,
			name: "服务商资料列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		addservice: {
			url: `${config.API_URL}/admin/enterpriseOp/save`,
			name: "添加服务商",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		updateservice: {
			url: `${config.API_URL}/admin/enterpriseOp/update`,
			name: "添加服务商",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		updateshop: {
			url: `${config.API_URL}/admin/enterprise/update`,
			name: "添加服务商",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},

		initserviceedits: {
			url: `${config.API_URL}/admin/enterpriseOp/edit`,
			name: "修改服务商初始化",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},

		initserviceedit: {
			url: `${config.API_URL}/admin/enterprise/edit`,
			name: "修改服务商初始化",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		initserviceadd: {
			url: `${config.API_URL}/admin/enterprise/add`,
			name: "添加服务商初始化",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		delservice: {
			url: `${config.API_URL}/admin/enterprise/delete`,
			name: "删除服务商",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		edirservicepass: {
			url: `${config.API_URL}/admin/enterprise/reset`,
			name: "重置服务商密码",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
	},
	elist:{
		shoplist: {
			url: `${config.API_URL}/admin/enterprise/list`,
			name: "店铺资料列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		addshanghu: {
			url: `${config.API_URL}/admin/enterprise/save`,
			name: "添加商户",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		chongzhi: {
			url: `${config.API_URL}/admin/enterprise/creditLine`,
			name: "商户充值",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		setdriver: {
			url: `${config.API_URL}/admin/enterprise/set`,
			name: "设置司机",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		driverlist:{
			url: `${config.API_URL}/admin/enterprise/driver`,
			name: "司机列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		settinglist:{
			url: `${config.API_URL}/admin/topic/edit`,
			name: "店铺资料列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		setting: {
			url: `${config.API_URL}/admin/topic/updat`,
			name: "商户充值",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		alipayshoplist:{
			url: `${config.API_URL}/admin/alipayShop/list`,
			name: "蚂蚁门店",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		alipayshopindex:{
			url: `${config.API_URL}/admin/alipayShop/index`,
			name: "蚂蚁门店初始化",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		daoru: {
			url: `${config.API_URL}/admin/alipayShop/sync_all`,
			name: "蚂蚁门店导入",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		alipayprogramindex:{
			url: `${config.API_URL}/admin/aliPay/index`,
			name: "支付宝小程序初始化",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		updata: {
			url: `${config.API_URL}/admin/aliPay/update`,
			name: "支付宝小程序更新",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		tongbu: {
			url: `${config.API_URL}/admin/enterprise/decorate`,
			name: "同步店铺",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		sheji: {
			url: `${config.API_URL}/admin/login/getToken`,
			name: "小程序设计",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		muban: {
			url: `${config.API_URL}/admin/topic/appletTemplate`,//enterprise/appletlisyy
			name: "小程序模板",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		selectmuban: {
			url: `${config.API_URL}/admin/enterprise/appletDecorate`,
			name: "选择模板",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		postbasic:{
			url: `${config.API_URL}/admin/topic/basic`,
			name: "商户基础资料",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		postswitch:{
			url: `${config.API_URL}/admin/topic/switch`,
			name: "商户开关设置",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		postapplet:{
			url: `${config.API_URL}/admin/topic/applet`,
			name: "商户微信小程序更新",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		postwechat:{
			url: `${config.API_URL}/admin/topic/weixin`,
			name: "商户微信公众号更新",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
	},
	efreight:{
		freightlist: {
			url: `${config.API_URL}/admin/freight/list`,
			name: "运费模板列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		delfreight:{
			url: `${config.API_URL}/admin/freight/delete`,
			name: "删除运费模板",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},

		add:{
            url: `${config.API_URL}/admin/freight/add`,
            name: "运费模板添加初始化",
            get: async function (params) {
                return await http.get(this.url, params);
            }
		},
		edit:{
			url: `${config.API_URL}/admin/freight/edit`,
			name: "运费模板添加初始化",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		savefreight:{
			url: `${config.API_URL}/admin/freight/save`,
			name: "添加运费模板",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		updatafreight:{
			url: `${config.API_URL}/admin/freight/update`,
			name: "编辑运费模板",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
	},
	// VIP设置
	vip:{
		list: {
			url: `${config.API_URL}/admin/vip/list`,
			name: "列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		add: {
			url: `${config.API_URL}/admin/vip/save`,
			name: "新增",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		update: {
			url: `${config.API_URL}/admin/vip/update`,
			name: "更新",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		delete: {
			url: `${config.API_URL}/admin/vip/delete`,
			name: "删除",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		send: {
			url: `${config.API_URL}/admin/vip/create`,
			name: "发卡",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		}
	},
	// 策略设置
	policy:{
		list: {
			url: `${config.API_URL}/admin/distribution/list`,
			name: "列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		add: {
			url: `${config.API_URL}/admin/distribution/add`,
			name: "新增",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		update: {
			url: `${config.API_URL}/admin/distribution/update`,
			name: "更新",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		delete: {
			url: `${config.API_URL}/admin/distribution/delete`,
			name: "删除",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		}
	}
}
