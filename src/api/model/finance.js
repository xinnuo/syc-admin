import config from "@/config"
import http from "@/utils/request"

export default {
	list: {
		url: `${config.API_URL}/admin/payment/list`,
		name: "收款单",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},
	plugin: {
		url: `${config.API_URL}/admin/payment/plugin`,
		name: "收款方式",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},
	sync: {
		url: `${config.API_URL}/admin/payment/update`,
		name: "更新状态",
		post: async function (data, config = {}) {
			return await http.post(this.url, data, config);
		}
	},
	refund: {
		list: {
			url: `${config.API_URL}/admin/refunds/list`,
			name: "退款单",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
	},
	cash: {
		list: {
			url: `${config.API_URL}/admin/transfer/list`,
			name: "提现单",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		submit: {
			url: `${config.API_URL}/admin/transfer/submit`,
			name: "人工打款",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		cancel: {
			url: `${config.API_URL}/admin/transfer/revert`,
			name: "撤销",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		sync: {
			url: `${config.API_URL}/admin/payment/update`,
			name: "更新状态",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
	},
}
