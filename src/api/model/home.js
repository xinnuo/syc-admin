import config from "@/config"
import http from "@/utils/request"

export default {
	refundCount: {
		url: `${config.API_URL}/admin/barrel_refund/count`,
		name: "退桶单数据",
		post: async function (params) {
			return await http.post(this.url, params);
		}
	},
	commonCount: {
		url: `${config.API_URL}/admin/common/index`,
		name: "店铺资产",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},
	cardCount: {
		url: `${config.API_URL}/admin/card/count`,
		name: "会员概况",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},
	todayCount: {
		url: `${config.API_URL}/admin/common/today`,
		name: "今日交易统计",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	daySevenCount: {
		url: `${config.API_URL}/admin/common/day7`,
		name: "7日交易统计",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},








}
