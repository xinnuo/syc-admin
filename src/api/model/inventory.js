import config from "@/config"
import http from "@/utils/request"

export default {
	// 服务商会员列表
	enterprise: {
		list: {
			url: `${config.API_URL}/admin/supplier/list`,
			name: "供销商",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		addshop: {
			url: `${config.API_URL}/admin/supplier/save`,
			name: "添加供应商",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		update: {
			url: `${config.API_URL}/admin//supplier/update`,
			name: "编辑供应商",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delete: {
			url: `${config.API_URL}/admin/supplier/delete`,
			name: "删除供应商",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		selectnum: {
			url: `${config.API_URL}/admin/supplier/fill_post`,
			name: "欠桶修改",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
				// id: 44 type:0金额修改 1桶修改 action:0加 1减  barrelId: 类型id  quantity: 数量 memo: 备注
			}
		},
		jilu: {
			url: `${config.API_URL}/admin/supplier/log_get`,
			name: "修改记录",
			get: async function (params) {
				return await http.get(this.url, params);
				// id: 44 type:0金额修改 1桶修改
			}
		},
		jiluexport: {
			url: `${config.API_URL}/admin/supplier/log_export`,
			name: "导出",
			get: async function (data, config = {responseType: 'blob'}) {
				return await http.get(this.url, data, config);
			}
		},
		detail: {
			url: `${config.API_URL}/admin/supplier/fill`,
			name: "详情",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
				// id: 44 type:0金额修改 1桶修改
			}
		},
		getpsi: {
			url: `${config.API_URL}/admin/psi/ps_index`,
			name: "供销商初始化接口",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		}


	},
	barrels: {
		list: {
			url: `${config.API_URL}/admin/psi/barrel_list`,
			name: "空桶库存",
			get: async function (params) {
				return await http.get(this.url, params);
			}

		},
		jiechunlist: {
			url: `${config.API_URL}/admin/psi/psb_summary`,
			name: "账面结存",
			get: async function (params) {
				return await http.get(this.url, params);
			}

		},
		kcrizhi: {
			url: `${config.API_URL}/admin/psi/ps_summary`,
			name: "库存日志",
			get: async function (params) {
				return await http.get(this.url, params);
			}

		},
		barrelexport: {
			url: `${config.API_URL}/admin/psi/ps_export`,
			name: "导出",
			get: async function (data, config = {responseType: 'blob'}) {
				return await http.get(this.url, data, config);
			}
		},
		barrellist: {
			url: `${config.API_URL}/admin/barrel/list`,
			name: "所有空桶列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		daorubarrel: {
			url: `${config.API_URL}/admin/psi/barrel_import`,
			name: "导入空桶",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}

		},
		delbarrel: {
			url: `${config.API_URL}/admin/shopDummy/delete_product`,
			name: "删除空桶",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}

		},
		selectbarrel: {
			url: `${config.API_URL}/admin/psi/ps_stock`,
			name: "修改空桶记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}

		},

	},
	product: {
		goodslist: {
			url: `${config.API_URL}/admin/psi/ps_list`,
			name: "商品库存",
			get: async function (params) {
				return await http.get(this.url, params);
			}

		},
		importgoodslist: {
			url: `${config.API_URL}/admin/shopDummy/import_list`,
			name: "导入商品列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}

		},
		selectbarrel: {
			url: `${config.API_URL}/admin/psi/ps_stock`,
			name: "修正商品库存",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}

		},
	daorugoods: {
		url: `${config.API_URL}/admin/shopDummy/product_import`,
		name: "导入商品",
		post: async function (data, config = {}) {
			return await http.post(this.url, data, config);
		}

	},

	},
	procure: {
		cgorderlist: {
			url: `${config.API_URL}/admin/psi/pc_list`,
			name: "采购入库列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}

		},

		coutnum: {
			url: `${config.API_URL}/admin/psi/count`,
			name: "数量",
			post: async function (params) {
				return await http.post(this.url, params);
			}
		},
		goodslist: {
			url: `${config.API_URL}/admin/psi/plist`,
			name: "采购入库商品列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		getfwsdetik: {
			url: `${config.API_URL}/admin/psi/pc_add`,
			name: "采购入库商品列表",
			get: async function () {
				return await http.get(this.url);
			}
		},
		list: {
			url: `${config.API_URL}/admin/productCategory/list`,
			name: "列表",
			get: async function (params) {
				return await http.get(this.url,params);
			}
		},
		procuremsg:{
			url: `${config.API_URL}/admin/psi/pc_view`,
			name: "列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}

		},
		upprocure:{
			url:`${config.API_URL}/admin/psi/pc_update`,
			name: "更新入库单",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},
		postprocure:{
			url:`${config.API_URL}/admin/psi/pc_save`,
			name: "上传入库单",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		},

		editprocuremsg:{
			url:`${config.API_URL}/admin/psi/pc_edit`,
			name: "获取库存修改详情",
			get: async function (data) {
				return await http.get(this.url, data);
			}
		},
		confirm:{
			url: `${config.API_URL}/admin/psi/pc_confirm`,
			name: "直接入库",
			post: async function (data, config = {}) {
				return await http.post(this.url, data,config);
			}
		}



	}

}
