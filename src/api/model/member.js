import config from "@/config"
import http from "@/utils/request"

export default {
	// 用户列表
	member: {
		list: {
			url: `${config.API_URL}/admin/member/list`,
			name: "用户列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		change: {
			url: `${config.API_URL}/admin/member/switch`,
			name: "用户过户",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		export: {
			url: `${config.API_URL}/admin/member/export`,
			name: "导出用户",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
	},

	// 会员信息
	card: {
		list: {
			url: `${config.API_URL}/admin/card/list`,
			name: "会员列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		info: {
			url: `${config.API_URL}/admin/card/detail`,
			name: "详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		save: {
			url: `${config.API_URL}/admin/card/save`,
			name: "添加会员",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		update: {
			url: `${config.API_URL}/admin/card/update`,
			name: "更新会员",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		updateField: {
			url: `${config.API_URL}/admin/card/updateParams`,
			name: "更新字段",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delete: {
			url: `${config.API_URL}/admin/card/delete`,
			name: "删除会员(假删)",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		promoter: {
			url: `${config.API_URL}/admin/card/promoters`,
			name: "推荐人",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		updatePromoter: {
			url: `${config.API_URL}/admin/card/updatePromoter`,
			name: "修改推荐人",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		reset: {
			url: `${config.API_URL}/admin/member/reset`,
			name: "重置密码",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		balance: {
			url: `${config.API_URL}/admin/card/update_balance_list`,
			name: "余额明细",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		updateBalance: {
			url: `${config.API_URL}/admin/card/update_balance`,
			name: "修改余额",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		input: {
			url: `${config.API_URL}/admin/card/importExcel`,
			name: "导入会员",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		export: {
			url: `${config.API_URL}/admin/card/export`,
			name: "导出会员",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		}
	},

	// 收货地址
	receiver: {
		list: {
			url: `${config.API_URL}/admin/receiver/list`,
			name: "收货地址列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		info: {
			url: `${config.API_URL}/admin/receiver/detail`,
			name: "详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		save: {
			url: `${config.API_URL}/admin/receiver/save`,
			name: "添加收货地址",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		update: {
			url: `${config.API_URL}/admin/receiver/update`,
			name: "修改收货地址",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delete: {
			url: `${config.API_URL}/admin/receiver/delete`,
			name: "删除收货地址",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		}
	},

	// 记账商品
	month: {
		list: {
			url: `${config.API_URL}/admin/card/monthly_product_list`,
			name: "记账列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		add: {
			url: `${config.API_URL}/admin/card/add_monthly_product`,
			name: "添加商品",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delete: {
			url: `${config.API_URL}/admin/card/delete_monthly_product`,
			name: "删除收货地址",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		}
	},

	// 电子水票
	ticket: {
		list: {
			url: `${config.API_URL}/admin/couponCode/list`,
			name: "列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		item: {
			url: `${config.API_URL}/admin/product/list`,
			name: "赠送列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		detail: {
			url: `${config.API_URL}/admin/couponCode/glideList`,
			name: "水票明细",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		add: {
			url: `${config.API_URL}/admin/couponCode/addT`,
			name: "赠送水票",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		use: {
			url: `${config.API_URL}/admin/couponCode/useT`,
			name: "核销水票",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		del: {
			url: `${config.API_URL}/admin/couponCode/delete`,
			name: "删除水票",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		}
	},

	// 空桶
	barrel: {
		list: {
			url: `${config.API_URL}/admin/barrel/barrel_stock_list`,
			name: "列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		detail: {
			url: `${config.API_URL}/admin/barrel/shippingBarrel_list.jhtml`,
			name: "空桶明细",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		add: {
			url: `${config.API_URL}/admin/barrel/addB`,
			name: "押桶",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		use: {
			url: `${config.API_URL}/admin/barrel/useB`,
			name: "退桶",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		borrow: {
			url: `${config.API_URL}/admin/barrel/borrow`,
			name: "退桶",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		}
	},

	// 电子发票
	receipt: {
		list: {
			url: `${config.API_URL}/admin/einvoice/list`,
			name: "列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		confirm: {
			url: `${config.API_URL}/admin/einvoice/confirm`,
			name: "确认",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		cancel: {
			url: `${config.API_URL}/admin/einvoice/cancel`,
			name: "取消",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		}
	},

	// 休眠会员
	dormant: {
		list: {
			url: `${config.API_URL}/admin/report/summary_sleep`,
			name: "列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		warn: {
			url: `${config.API_URL}/admin/cardVisit/remind_push`,
			name: "提醒",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		vList: {
			url: `${config.API_URL}/admin/cardVisit/list`,
			name: "回访列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		vAdd: {
			url: `${config.API_URL}/admin/cardVisit/add`,
			name: "添加回访",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		}
	},

	// 推广申请
	agent: {
		list: {
			url: `${config.API_URL}/admin/cardReqVip/list`,
			name: "列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		update: {
			url: `${config.API_URL}/admin/cardReqVip/update`,
			name: "修改类型",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		}
	},

	// 叫水提醒
	water: {
		list: {
			url: `${config.API_URL}/admin/card/remind_detail`,
			name: "列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		update: {
			url: `${config.API_URL}/admin/card/remind_push`,
			name: "提醒",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		}
	},

	// 评价管理
	rate: {
		list: {
			url: `${config.API_URL}/admin/articleReview/list`,
			name: "列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		update: {
			url: `${config.API_URL}/admin/articleReview/update`,
			name: "修改",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delete: {
			url: `${config.API_URL}/admin/articleReview/delete`,
			name: "删除",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		}
	}
}
