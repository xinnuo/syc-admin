import config from "@/config"
import http from "@/utils/request"

export default {
	orderIndex: {
		url: `${config.API_URL}/admin/order/index`,
		name: "销售订单数据初始化",
		get: async function(params){
			return await http.get(this.url, params);
		}
	},
	list: {
		url: `${config.API_URL}/admin/order/list`,
		name: "销售订单",
		get: async function(params){
			return await http.get(this.url, params);
		}
	},
	plist: {
		url: `${config.API_URL}/admin/order/plist`,
		name: "推广订单",
		get: async function(params){
			return await http.get(this.url, params);
		}
	},
	count:{
		url: `${config.API_URL}/admin/order/count`,
		name: "订单统计数",
		post: async function(params){
			return await http.post(this.url, params);
		}
	},

	view:{
		url: `${config.API_URL}/admin/order/view`,
		name: "订单详情",
		get: async function(params){
			return await http.get(this.url, params);
		}
	},
	cancel:{
		url: `${config.API_URL}/admin/order/cancel`,
		name: "订单关闭",
		post: async function(params){
			return await http.post(this.url, params);
		}
	},

	confirmData:{
		url: `${config.API_URL}/admin/order/confirm`,
		name: "订单确认初始化数据",
		get: async function(params){
			return await http.get(this.url, params);
		}
	},
	confirm:{
		url: `${config.API_URL}/admin/order/confirm`,
		name: "订单确认",
		post: async function(params){
			return await http.post(this.url, params);
		}
	},
	shippingData:{
		url: `${config.API_URL}/admin/order/shipping`,
		name: "订单发货、订单转派页面初始化数据",
		get: async function(params){
			return await http.get(this.url, params);
		}
	},

	shipping:{
		url: `${config.API_URL}/admin/order/shipping`,
		name: "订单发货确定",
		post: async function(params){
			return await http.post(this.url, params);
		}
	},

	dispatch:{
		url: `${config.API_URL}/admin/shipping/dispatch`,
		name: "订单转派确定",
		post: async function(params){
			return await http.post(this.url, params);
		}
	},

	refunds:{
		url: `${config.API_URL}/admin/order/refunds`,
		name: "订单退款",
		post: async function(params){
			return await http.post(this.url, params);
		}
	},
	returns:{
		url: `${config.API_URL}/admin/order/returns`,
		name: "退货退款/同意退货退款",
		post: async function(params){
			return await http.post(this.url, params);
		}
	},

	forceReturns:{
		url: `${config.API_URL}/admin/order/forceReturns`,
		name: "退货退款2",
		post: async function(params){
			return await http.post(this.url, params);
		}
	},

	unReturns:{
		url: `${config.API_URL}/admin/order/unReturns`,
		name: "订单撤销退货退款",
		post: async function(params){
			return await http.post(this.url, params);
		}
	},
	deliveryIndex:{
		url: `${config.API_URL}/admin/shipping/delivery_index`,
		name: "订单送达初始化数据",
		get: async function(params){
			return await http.get(this.url, params);
		}
	},

	delivery:{
		url: `${config.API_URL}/admin/shipping/delivery`,
		name: "订单送达确认",
		post: async function(params){
			return await http.post(this.url, params);
		}
	},
	shippRemind:{
		url: `${config.API_URL}/admin/order/shipp_remind`,
		name: "订单送货提醒",
		post: async function(params){
			return await http.post(this.url, params);
		}
	},

	shippingConfirm:{
		url: `${config.API_URL}/admin/shipping/shippingConfirm`,
		name: "订单核销初始化数据",
		get: async function(params){
			return await http.get(this.url, params);
		}
	},

	shippingCompleted:{
		url: `${config.API_URL}/admin/shipping/completed`,
		name: "订单核销确认",
		post: async function(params){
			return await http.post(this.url, params);
		}
	},

	reject:{
		url: `${config.API_URL}/admin/order/reject`,
		name: "订单驳回确认",
		post: async function(params){
			return await http.post(this.url, params);
		}
	},

	add: {
		url: `${config.API_URL}/admin/order/add`,
		name: "补单",
		get: async function (params) {
			return await http.get(this.url, params);
		}
	},

	batchShipping:{
		url: `${config.API_URL}/admin/order/batch_shipping`,
		name: "订单批量发货",
		post: async function(params){
			return await http.post(this.url, params);
		}
	},
	batchPrint:{
		url: `${config.API_URL}/admin/order/batch_print`,
		name: "订单批量打印小票",
		post: async function(params){
			return await http.post(this.url, params);
		}
	},

	export:{
		url: `${config.API_URL}/admin/order/export`,
		name: "订单导出报表",
		get: async function (data, config = {responseType: 'blob'}) {
			return await http.get(this.url, data, config);
		}
	},


	calculate:{
		url: `${config.API_URL}/admin/order/calculate`,
		name: "计算",
		get: async function(params){
			return await http.post(this.url, params);
		}
	},
	shopList:{
		url: `${config.API_URL}/admin/order/shop_list_all`,
		name: "配送站点",
		get: async function(params){
			return await http.get(this.url, params);
		}
	},
	adminGet:{
		url: `${config.API_URL}/admin/order/admin_get`,
		name: "送货人员",
		get: async function(params){
			return await http.post(this.url, params);
		}
	},

	callRecord:{
		list: {
			url: `${config.API_URL}/admin/callRecord/list`,
			name: "呼叫记录",
			get: async function(params){
				return await http.get(this.url, params);
			}
		},
		save: {
			url: `${config.API_URL}/admin/callRecord/save`,
			name: "保存记录",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
	},

	menual:{
		save: {
			url: `${config.API_URL}/admin/order/save`,
			name: "新增人工报单",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
	},

	barrelRefund:{
		list: {
			url: `${config.API_URL}/admin/barrel_refund/list`,
			name: "退桶单列表",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},

		cancel: {
			url: `${config.API_URL}/admin/barrel_refund/cancel`,
			name: "关闭退桶单",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},


		shipping: { //派单数据初始化接口
			url: `${config.API_URL}/admin/barrel_refund/shipping`,
			name: "派单",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},

		//派单、转派接口一样
		dispatch: {
			url: `${config.API_URL}/admin/barrel_refund/dispatch`,
			name: "派单确定",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		receive: {
			url: `${config.API_URL}/admin/barrel_refund/receive`,
			name: "核销",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},

		refunds: {
			url: `${config.API_URL}/admin/barrel_refund/refunds`,
			name: "线上退款",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},

		export: {
			url: `${config.API_URL}/admin/barrel_refund/export`,
			name: "退桶单导出报表",
			get: async function (data, config = {responseType: 'blob'}) {
				return await http.get(this.url, data, config);
			}
		},

		add: {
			url: `${config.API_URL}/admin/barrel_refund/add`,
			name: "新增退桶单初始化接口",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},

		save: {
			url: `${config.API_URL}/admin/barrel_refund/save`,
			name: "提交新增退桶单",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},



	},


	barrels:{
		url: `${config.API_URL}/admin/barrel/barrel_stock_list`,
		name: "退桶品牌",
		get: async function (data, config = {}) {
			return await http.get(this.url, data, config);
		}

	},

	// 空桶统计 type='mortgage' 为押桶类型
	barrelStock:{
		list:{
			url: `${config.API_URL}/admin/barrel_stock/list`,
			name: "空桶统计",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},

	},

	plugin:{
		url: `${config.API_URL}/payment/plugin`,
		name: "付款方式",
		get: async function(params){
			return await http.get(this.url, params);
		}
	},



}
