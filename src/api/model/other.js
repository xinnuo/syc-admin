import config from "@/config"
import http from "@/utils/request"

export default {
	area: {
		list: {
			url: `${config.API_URL}/admin/common/area`,
			name: "省市区",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		query: {
			url: `${config.API_URL}/lbs/geoQuery`,
			name: "关键字查询",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		}
	},
	// 二维码生成
	qrcode: {
		list: {
			url: `${config.API_URL}/admin/qrcode/list`,
			name: "二维码列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		add: {
			url: `${config.API_URL}/admin/qrcode/save`,
			name: "添加二维码",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		export: {
			url: `${config.API_URL}/admin/qrcode/export`,
			name: "导出二维码",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
		delete: {
			url: `${config.API_URL}/admin/qrcode/delete`,
			name: "删除二维码",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
	},
	// 短信管理
	sms: {
		list: {
			url: `${config.API_URL}/admin/smssend/list`,
			name: "短信列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		add: {
			url: `${config.API_URL}/admin/smssend/save`,
			name: "发送短信",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
	},
	// 快递公司
	express: {
		list: {
			url: `${config.API_URL}/admin/deliveryCorp/list`,
			name: "快递列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		add: {
			url: `${config.API_URL}/admin/deliveryCorp/save`,
			name: "新增快递",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		update: {
			url: `${config.API_URL}/admin/deliveryCorp/update`,
			name: "编辑快递",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delete: {
			url: `${config.API_URL}/admin/deliveryCorp/delete`,
			name: "删除快递",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
	},
	// 消息中心
	message: {
		list: {
			url: `${config.API_URL}/admin/message/list`,
			name: "消息列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		count: {
			url: `${config.API_URL}/admin/common/message`,
			name: "消息数量",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		read: {
			url: `${config.API_URL}/admin/message/readAll`,
			name: "标记已读",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		}
	},
	// 帮助中心
	help: {
		list: {
			url: `${config.API_URL}/admin/article/list`,
			name: "帮助列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		add: {
			url: `${config.API_URL}/admin/article/save`,
			name: "新增",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		update: {
			url: `${config.API_URL}/admin/article/update`,
			name: "修改",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delete: {
			url: `${config.API_URL}/admin/article/delete`,
			name: "删除",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
	}
}
