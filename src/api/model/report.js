import config from "@/config"
import http from "@/utils/request"

export default {
	pickup: {
		list: {
			url: `${config.API_URL}/admin/phCredit/list`,
			name: "自提空桶列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		barrel: {
			url: `${config.API_URL}/admin/phCredit/barrel_list`,
			name: "空桶品牌",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		add: {
			url: `${config.API_URL}/admin/phCredit/barrel_save`,
			name: "添加空桶",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		info: {
			url: `${config.API_URL}/admin/phCredit/detail`,
			name: "入库详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		calc: {
			url: `${config.API_URL}/admin/phOrder/calculate`,
			name: "开单商品计算",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		save: {
			url: `${config.API_URL}/admin/phCredit/save`,
			name: "确认收款",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
	},
	orders: {
		list: {
			url: `${config.API_URL}/admin/phOrder/list`,
			name: "订单列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		add: {
			url: `${config.API_URL}/admin/phOrder/save`,
			name: "人工报单",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		info: {
			url: `${config.API_URL}/admin/phOrder/detail`,
			name: "订单详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		export: {
			url: `${config.API_URL}/admin/phOrder/export`,
			name: "人工报单",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
		revoke: {
			url: `${config.API_URL}/admin/phOrder/revoke`,
			name: "关闭订单",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		confirm: {
			url: `${config.API_URL}/admin/phOrder/confirm`,
			name: "确认订单",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		shop: {
			url: `${config.API_URL}/admin/phOrder/shippingShop`,
			name: "配送站点",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		ship: {
			url: `${config.API_URL}/admin/phOrder/shipping`,
			name: "订单发货",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		print: {
			url: `${config.API_URL}/admin/phOrder/print`,
			name: "打单完成",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		cfmBarrel: {
			url: `${config.API_URL}/admin/phShipping/barrel_confirm`,
			name: "确认回桶",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		cfmPay: {
			url: `${config.API_URL}/admin/phShipping/payment_confirm`,
			name: "确认收款",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		complete: {
			url: `${config.API_URL}/admin/phOrder/complete`,
			name: "完成订单",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
	},
	product: {
		list: {
			url: `${config.API_URL}/admin/phStock/plist`,
			name: "商品列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
	},
	fill: {
		list: {
			url: `${config.API_URL}/admin/phFill/list`,
			name: "库存审核",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		confirm: {
			url: `${config.API_URL}/admin/phFill/confirm`,
			name: "确认审核",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		cancel: {
			url: `${config.API_URL}/admin/phFill/cancel`,
			name: "取消审核",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
	},
	stock: {
		summary: {
			url: `${config.API_URL}/admin/phStock/enterprise_summary`,
			name: "资产统计",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		export: {
			url: `${config.API_URL}/admin/phStock/enterprise_export`,
			name: "导出资产",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
		arrear: {
			url: `${config.API_URL}/admin/phStock/updateArrears`,
			name: "修改欠款",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		aDetail: {
			url: `${config.API_URL}/admin/phStock/arrears_detail`,
			name: "欠款明细",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		aExport: {
			url: `${config.API_URL}/admin/phStock/arrears_detail_export`,
			name: "导出欠款",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
		barrel: {
			url: `${config.API_URL}/admin/phStock/barrel_stock_list`,
			name: "空桶资产",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		bDetail: {
			url: `${config.API_URL}/admin/phStock/barrel_detail`,
			name: "空桶明细",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		bdExpord: {
			url: `${config.API_URL}/admin/phStock/barrel_detail_export`,
			name: "导出空桶明细",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
		bExport: {
			url: `${config.API_URL}/admin/phStock/barrel_stock_list_export`,
			name: "导出空桶",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
		uBarrel: {
			url: `${config.API_URL}/admin/phStock/updateBarrelStock`,
			name: "欠/还/押/退桶",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		plist: {
			url: `${config.API_URL}/admin/phStock/plist.jhtml`,
			name: "优惠商品",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		gSummary: {
			url: `${config.API_URL}/admin/phStock/good_summary`,
			name: "库存明细",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		gExport: {
			url: `${config.API_URL}/admin/phStock/good_export`,
			name: "导出明细",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
		pSummary: {
			url: `${config.API_URL}/admin/phReport/phStock_summary`,
			name: "预购库存",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		pExport: {
			url: `${config.API_URL}/admin/phReport/phStock_export`,
			name: "导出预购",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
	},
	order: {
		summary: {
			url: `${config.API_URL}/admin/phReport/order_summary`,
			name: "进货统计",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		detail: {
			url: `${config.API_URL}/admin/phReport/order_detail`,
			name: "进货详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		product: {
			url: `${config.API_URL}/admin/phReport/product_list`,
			name: "商品列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		export: {
			url: `${config.API_URL}/admin/phReport/order_export`,
			name: "导出进货",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
		dExport: {
			url: `${config.API_URL}/admin/phReport/order_detail_export`,
			name: "导出明细",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
	},
	driver: {
		summary: {
			url: `${config.API_URL}/admin/phReport/driver_summary`,
			name: "送货统计",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		detail: {
			url: `${config.API_URL}/admin/phReport/driver_detail`,
			name: "送货详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		export: {
			url: `${config.API_URL}/admin/phReport/driver_export`,
			name: "导出送货",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		}
	},
	barrel: {
		summary: {
			url: `${config.API_URL}/admin/phReport/barrel_summary`,
			name: "空桶统计",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		detail: {
			url: `${config.API_URL}/admin/phReport/barrel_detail`,
			name: "空桶明细",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		export: {
			url: `${config.API_URL}/admin/phReport/barrel_export`,
			name: "导出空桶",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
		dExport: {
			url: `${config.API_URL}/admin/phReport/barrel_detail_export`,
			name: "导出空桶明细",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
		eSummary: {
			url: `${config.API_URL}/admin/phReport/e_barrel_summary`,
			name: "水桶统计",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		eExport: {
			url: `${config.API_URL}/admin/phReport/e_barrel_export`,
			name: "导出水桶",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
	},
	payment: {
		list: {
			url: `${config.API_URL}/admin/phReport/payment_summary`,
			name: "收款统计",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		export: {
			url: `${config.API_URL}/admin/phReport/payment_export`,
			name: "导出收款",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
		month: {
			url: `${config.API_URL}/admin/phReport/payment_monthly`,
			name: "收款月报",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		detail: {
			url: `${config.API_URL}/admin/phReport/payment_detail`,
			name: "收款明细",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		dExport: {
			url: `${config.API_URL}/admin/phReport/payment_detail_export`,
			name: "导出明细",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
		mExport: {
			url: `${config.API_URL}/admin/phReport/payment_monthly_export`,
			name: "导出月报",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
	},
	goods: {
		summary: {
			url: `${config.API_URL}/admin/phReport/goods_summary`,
			name: "商品统计",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		export: {
			url: `${config.API_URL}/admin/phReport/goods_export`,
			name: "导出商品",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
		sSummary: {
			url: `${config.API_URL}/admin/phStock/good_summary`,
			name: "商品库存明细",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		sUpdate: {
			url: `${config.API_URL}/admin/phStock/update`,
			name: "修改商品库存",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		sVerify: {
			url: `${config.API_URL}/admin/phStock/verify_post`,
			name: "发起商品库存审核",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		sExport: {
			url: `${config.API_URL}/admin/phStock/good_export`,
			name: "导出库存明细",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
	},
	sale: {
		summary: {
			url: `${config.API_URL}/admin/phReport/sale_summary`,
			name: "销量统计",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		export: {
			url: `${config.API_URL}/admin/phReport/sale_export`,
			name: "导出销量",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
	},
	coupon: {
		summary: {
			url: `${config.API_URL}/admin/phReport/coupon_summary`,
			name: "优惠券统计",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		export: {
			url: `${config.API_URL}/admin/phReport/coupon_export`,
			name: "导出优惠券",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
		info: {
			url: `${config.API_URL}/admin/phStock/coupon_summary`,
			name: "优惠券明细",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		detail: {
			url: `${config.API_URL}/admin/phStock/getEnterpriseCoupon`,
			name: "服务商优惠券",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		iExport: {
			url: `${config.API_URL}/admin/phStock/coupon_export`,
			name: "导出明细",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
		fill: {
			url: `${config.API_URL}/admin/phStock/fill_post`,
			name: "修改库存",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
	},
	other: {
		list: {
			url: `${config.API_URL}/admin/phPayment/other_list`,
			name: "其他收款",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		save: {
			url: `${config.API_URL}/admin/phPayment/other_save`,
			name: "添加收款",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		export: {
			url: `${config.API_URL}/admin/phPayment/other_export`,
			name: "导出收款",
			get: async function (data, config = {}) {
				return await http.get(this.url, data, config);
			}
		},
	},
}
