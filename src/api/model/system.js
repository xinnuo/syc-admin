import config from "@/config"
import http from "@/utils/request"

export default {
	// 导航管理
	navigation: {
		list: {
			url: `${config.API_URL}/admin/navigation/list`,
			name: "列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		tag: {
			url: `${config.API_URL}/admin/navigation/add`,
			name: "导航标签",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		add: {
			url: `${config.API_URL}/admin/navigation/save`,
			name: "新增",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delete: {
			url: `${config.API_URL}/admin/navigation/delete`,
			name: "删除",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		update: {
			url: `${config.API_URL}/admin/navigation/update`,
			name: "修改",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		}
	},
	// 宣传素材
	spread: {
		list: {
			url: `${config.API_URL}/admin/spread/list`,
			name: "列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		add: {
			url: `${config.API_URL}/admin/spread/save`,
			name: "新增",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delete: {
			url: `${config.API_URL}/admin/spread/delete`,
			name: "删除",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		update: {
			url: `${config.API_URL}/admin/spread/update`,
			name: "修改",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		}
	},
	// 链接管理
	link: {
		list: {
			url: `${config.API_URL}/admin/ad/list`,
			name: "列表",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		add: {
			url: `${config.API_URL}/admin/ad/save`,
			name: "新增",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delete: {
			url: `${config.API_URL}/admin/ad/delete`,
			name: "删除",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		update: {
			url: `${config.API_URL}/admin/ad/update`,
			name: "修改",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		tag: {
			url: `${config.API_URL}/admin/ad/tag`,
			name: "标签",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		position: {
			url: `${config.API_URL}/admin/ad/position`,
			name: "位置",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
	},
	// 菜单管理
	menu: {
		myMenus: {
			url: `${config.API_URL}/admin/power/my`,
			name: "登录菜单",
			get: async function(){
				return await http.get(this.url);
			}
		},
		list: {
			url: `${config.API_URL}/admin/power/list`,
			name: "菜单列表",
			get: async function(){
				return await http.get(this.url);
			}
		},
		add: {
			url: `${config.API_URL}/admin/power/save`,
			name: "新增菜单",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delete: {
			url: `${config.API_URL}/admin/power/delete`,
			name: "删除菜单",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		update: {
			url: `${config.API_URL}/admin/power/update`,
			name: "更新菜单",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		valid: {
			url: `${config.API_URL}/admin/power/check`,
			name: "校验",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		}
	},
	// 行政区域
	area: {
		list: {
			url: `${config.API_URL}/admin/area/list`,
			name: "区域列表",
			get: async function(){
				return await http.get(this.url);
			}
		},
		info: {
			url: `${config.API_URL}/admin/area/edit`,
			name: "区域详情",
			get: async function (params) {
				return await http.get(this.url, params);
			}
		},
		add: {
			url: `${config.API_URL}/admin/area/save`,
			name: "新增区域",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		delete: {
			url: `${config.API_URL}/admin/area/delete`,
			name: "删除区域",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
		update: {
			url: `${config.API_URL}/admin/area/update`,
			name: "修改区域",
			post: async function (data, config = {}) {
				return await http.post(this.url, data, config);
			}
		},
	}
}
