// 非分页数据配置
export default {
	successCode: 'success',          //请求完成响应
	pageSize: 15,			         //表格每一页条数
	pageSizes: [15, 20, 30, 40, 50], //表格可设置的一页条数
	size_20: 20,			         //表格每一页条数
	size_20_list: [20, 30, 40, 50],  //表格可设置的一页条数
	parseData: function (res) {
		return {
			code: res.type,	 //分析状态字段结构
			data: res.data,	 //分析数据字段结构
			msg: res.content //分析描述字段结构
		}
	}
}
