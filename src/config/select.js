//字典选择器配置

export default {
	parseData: function (res) {
		return {
			code: res.type,	 //分析状态字段结构
			data: res.data,	 //分析数据字段结构
			msg: res.content //分析描述字段结构
		}
	},
	request: {
		name: 'searchValue' //规定搜索字段
	}
}
