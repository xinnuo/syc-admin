/**
 * 数组方式封装队列
 * shift()数组，数据都需要向前补齐，效率较低
 */
class ArrayQueue {
	constructor() {
		this.queue = [];
	}

	// 入队
	enqueue(value) {
		return this.queue.push(value);
	}

	// 出队
	dequeue() {
		return this.queue.shift() || null;
	}

	// 取队头元素
	peek() {
		return this.queue[0] || null;
	}

	// 判断队列是否为空
	isEmpty() {
		return this.queue.length === 0;
	}

	// 取队列有多少个元素
	size() {
		return this.queue.length;
	}

	// 清空队列
	clear() {
		this.queue.splice(0, this.queue.length);
	}

	toString() {
		return this.queue.join(' ');
	}
}

export default ArrayQueue
