const range = [
	{
		text: '今日',
		value: () => {
			const now = new Date()
			return [now, now]
		}
	},
	{
		text: '昨日',
		value: () => {
			const now = new Date()
			now.setDate(now.getDate() - 1)
			return [now, now]
		}
	},
	{
		text: '本月',
		value: () => {
			const end = new Date()
			const start = new Date()
			start.setDate(1)
			return [start, end]
		}
	},
	{
		text: '最近一周',
		value: () => {
			const end = new Date()
			const start = new Date()
			start.setDate(start.getDate() - 6)
			return [start, end]
		}
	},
	{
		text: '最近一个月',
		value: () => {
			const end = new Date()
			const start = new Date()
			start.setMonth(start.getMonth() - 1)
			return [start, end]
		}
	},
	{
		text: '最近三个月',
		value: () => {
			const end = new Date()
			const start = new Date()
			start.setMonth(start.getMonth() - 3)
			return [start, end]
		}
	}
];

export default range
