/**
 * Vue3全局事件总线
 */
type IBus = {
	$on: (name: string, callback: Function) => void;
	$emit: (name: string) => void;
};

type IName = string | number | symbol;

interface IList {
	[name: IName]: Array<Function>;
}

class Bus implements IBus {
	list: IList;

	constructor() {
		this.list = {};
	}

	$emit(name: string, ...args: any) {
		const eventFn: Array<Function> = this.list[name];
		eventFn?.forEach((item) => {
			item.apply(this, args);
		});
	}

	$on(name: string, callback: Function) {
		const fn = this.list[name] || [];
		fn.push(callback);
		this.list[name] = fn;
	}
}

export default new Bus();
