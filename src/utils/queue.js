/**
 * 封装队列，先进先出
 */
export default function Queue() {
	// 属性
	this.items = [];

	// 将元素加入到队列中
	Queue.prototype.enqueue = function (element) {
		this.items.push(element);
	}

	// 从队列中删除前端元素
	Queue.prototype.dequeue = function () {
		return this.items.shift() || null;
	}

	// 查看前端的元素
	Queue.prototype.front = function () {
		return this.items[0] || null;
	}

	// 查看队列是否为空
	Queue.prototype.isEmpty = function () {
		return this.items === 0;
	}

	// 查看队列中元素的个数
	Queue.prototype.size = function () {
		return this.items.length;
	}

	// toString方法
	Queue.prototype.toString = function () {
		return this.items.join(',');
	}

}
