/**
 * 封装栈类，后进先出
 * 创建一个栈：const stack = new Stack()
 */
class Stack {
	constructor() {
		this.item = [];
	}

	// push()将元素压入栈，并且添加的元素只能放在栈顶（也就是数组的尾部）
	push(element) {
		this.item.push(element);
	}

	// pop()从栈中去元素，移除的就只能是栈顶的元素
	pop() {
		return this.item.pop();
	}

	// peek()查看一下栈顶的元素
	peek() {
		return this.item[this.item.length - 1];
	}

	// isEmpty()判断栈是否为空
	isEmpty() {
		return this.item.length === 0;
	}

	// size方法获取栈中元素的个数
	size() {
		return this.item.length;
	}

	// toString()方法
	toString() {
		return this.item.join(' ');
	}

	// clear()清空栈中的所有的元素
	clear() {
		return this.item = [];
	}
}

export default Stack
