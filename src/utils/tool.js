/*
 * @Descripttion: 工具集
 * @version: 1.2
 * @LastEditors: sakuya
 * @LastEditTime: 2022年5月24日00:28:56
 */

import CryptoJS from 'crypto-js';
import sysConfig from "@/config";

const tool = {}

/* localStorage */
tool.data = {
	set(key, data, datetime = 0) {
		//加密
		if (sysConfig.LS_ENCRYPTION == "AES") {
			data = tool.crypto.AES.encrypt(JSON.stringify(data), sysConfig.LS_ENCRYPTION_key)
		}
		let cacheValue = {
			content: data,
			datetime: parseInt(datetime) === 0 ? 0 : new Date().getTime() + parseInt(datetime) * 1000
		}
		return localStorage.setItem(key, JSON.stringify(cacheValue))
	},
	get(key) {
		try {
			const value = JSON.parse(localStorage.getItem(key))
			if (value) {
				let nowTime = new Date().getTime()
				if (nowTime > value.datetime && value.datetime != 0) {
					localStorage.removeItem(key)
					return null;
				}
				//解密
				if (sysConfig.LS_ENCRYPTION == "AES") {
					value.content = JSON.parse(tool.crypto.AES.decrypt(value.content, sysConfig.LS_ENCRYPTION_key))
				}
				return value.content
			}
			return null
		} catch (err) {
			return null
		}
	},
	remove(key) {
		return localStorage.removeItem(key)
	},
	clear() {
		return localStorage.clear()
	}
}

/*sessionStorage*/
tool.session = {
	set(table, settings) {
		var _set = JSON.stringify(settings)
		return sessionStorage.setItem(table, _set);
	},
	get(table) {
		var data = sessionStorage.getItem(table);
		try {
			data = JSON.parse(data)
		} catch (err) {
			return null
		}
		return data;
	},
	remove(table) {
		return sessionStorage.removeItem(table);
	},
	clear() {
		return sessionStorage.clear();
	}
}

/*cookie*/
tool.cookie = {
	set(name, value, config = {}) {
		var cfg = {
			expires: null,
			path: null,
			domain: null,
			secure: false,
			httpOnly: false,
			...config
		}
		// var cookieStr = `${name}=${escape(value)}`
		var cookieStr = `${name}=${encodeURI(value)}`
		if (cfg.expires) {
			var exp = new Date()
			exp.setTime(exp.getTime() + parseInt(cfg.expires) * 1000)
			cookieStr += `;expires=${exp.toGMTString()}`
		}
		if (cfg.path) {
			cookieStr += `;path=${cfg.path}`
		}
		if (cfg.domain) {
			cookieStr += `;domain=${cfg.domain}`
		}
		document.cookie = cookieStr
	},
	get(name) {
		var arr = document.cookie.match(new RegExp("(^| )" + name + "=([^;]*)(;|$)"))
		if (arr != null) {
			// return unescape(arr[2])
			return decodeURI(arr[2])
		} else {
			return null
		}
	},
	remove(name) {
		var exp = new Date()
		exp.setTime(exp.getTime() - 1)
		document.cookie = `${name}=;expires=${exp.toGMTString()}`
	}
}

/* Fullscreen */
/* 是否支持全屏 */
tool.isFullscreenEnabled = () => {
	let requestFullscreen = document.body.requestFullscreen
		|| document.body.webkitRequestFullscreen
		|| document.body.mozRequestFullScreen
		|| document.body.msRequestFullscreen;

	var fullscreenEnabled = document.fullscreenEnabled
		|| document.mozFullScreenEnabled
		|| document.webkitFullscreenEnabled
		|| document.msFullscreenEnabled;

	return !!(requestFullscreen && fullscreenEnabled);
}

/* 获取当前全屏的元素 */
tool.fullscreenElement = () => {
	return (document.fullscreenElement
		|| document.webkitFullscreenElement
		|| document.msFullscreenElement
		|| document.mozFullScreenElement
		|| null);
}

/* 判断当前是否全屏 */
tool.isFullscreen = () => {
	return !!(document.webkitIsFullScreen || tool.fullscreenElement());
}

/* 全屏 */
tool.fullscreen = element => {
	if (tool.isFullscreen()) {
		if (element === tool.fullscreenElement()) return;
	}

	if (element.requestFullscreen) {
		element.requestFullscreen();
	} else if (element.mozRequestFullScreen) {
		element.mozRequestFullScreen();
	} else if (element.webkitRequestFullscreen) {
		element.webkitRequestFullscreen();
	} else if (element.msRequestFullscreen) {
		element.msRequestFullscreen();
	}

	return this;
}

/* 退出全屏 */
tool.exitFullscreen = () => {
	if (document.exitFullscreen) {
		document.exitFullscreen();
	} else if (document.mozCancelFullScreen) {
		document.mozCancelFullScreen();
	} else if (document.webkitExitFullscreen) {
		document.webkitExitFullscreen();
	} else if (document.msExitFullscreen) {
		document.msExitFullscreen();
	}

	return this;
}

/* 触发全屏，如果当前全屏的元素不是指定的元素，则取消全屏无效 */
tool.toggleFullscreen = (el, callback) => {
	if (tool.isFullscreen()) {
		var element = tool.fullscreenElement();
		if (el === element) tool.exitFullscreen();
	} else {
		tool.fullscreen(el);
	}

	if (typeof callback != 'undefined' && callback != null) {
		callback(!tool.isFullscreen());
	}

	return this;
}

tool.screen = element => {
	let isFull = tool.isFullscreen();
	if (isFull) {
		if (document.exitFullscreen) {
			document.exitFullscreen();
		} else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.webkitExitFullscreen) {
			document.webkitExitFullscreen();
		}
	} else {
		if (element.requestFullscreen) {
			element.requestFullscreen();
		} else if (element.msRequestFullscreen) {
			element.msRequestFullscreen();
		} else if (element.mozRequestFullScreen) {
			element.mozRequestFullScreen();
		} else if (element.webkitRequestFullscreen) {
			element.webkitRequestFullscreen();
		}
	}
}

/* 获取本地File对象路径 */
tool.fileURL = file => {
	let url = null;
	if (window.createObjectURL != undefined) { // basic
		url = window.createObjectURL(file);
	} else if (window.webkitURL != undefined) { // webkit or chrome
		url = window.webkitURL.createObjectURL(file);
	} else if (window.URL != undefined) { // mozilla(firefox)
		url = window.URL.createObjectURL(file);
	}
	return url;
}

/* 判断字符是否为空 */
tool.isNull = res => {
	return typeof res == "undefined" || res == null || res == '';
}

/* 判断是否为网址 */
tool.isUrl = url => {
	let regex = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w.-]+)+[\w\-._~:/?#[\]@!$&'*+,;=]+$/
	return regex.test(url)
}

/* 复制对象 */
tool.objCopy = obj => JSON.parse(JSON.stringify(obj));


/* 日期格式化 */
tool.dateFormat = (date, fmt = 'yyyy-MM-dd hh:mm:ss') => {
	if (date == null || date == '') {
		return '';
	}
	date = new Date(date)
	var o = {
		"M+": date.getMonth() + 1,               //月份
		"d+": date.getDate(),                    //日
		"h+": date.getHours(),                   //小时
		"m+": date.getMinutes(),                 //分
		"s+": date.getSeconds(),                 //秒
		"q+": Math.floor((date.getMonth() + 3) / 3), //季度
		"S": date.getMilliseconds()             //毫秒
	};

	const re = /(y+)/;
	if (re.test(fmt)) {
		const t = re.exec(fmt)[1];
		fmt = fmt.replace(t, (date.getFullYear() + "").substring(4 - t.length));
	}
	for (var k in o) {
		const regx = new RegExp("(" + k + ")");
		if (regx.test(fmt)) {
			const t = regx.exec(fmt)[1];
			fmt = fmt.replace(t, (t.length == 1) ? (o[k]) : (("00" + o[k]).substring(("" + o[k]).length)));
		}
	}

	return fmt;
}

/* 比较时间相差天数 */
tool.daysDiff = (oldDate, newDate) => {
	let d1 = new Date(oldDate);
	let d2 = new Date(newDate);

	let timeDiff = Math.abs(d2.getTime() - d1.getTime());
	return Math.floor(timeDiff / (1000 * 60 * 60 * 24));
}

/* 数字千分符 */
tool.groupSeparator = (num) => {
	num = num + '';
	if (!num.includes('.')) {
		num += '.'
	}
	return num.replace(/(\d)(?=(\d{3})+\.)/g, function ($0, $1) {
		return $1 + ',';
	}).replace(/\.$/, '');
}

/* 常用加解密 */
tool.crypto = {
	//MD5加密
	MD5(data) {
		return CryptoJS.MD5(data).toString()
	},
	//BASE64加解密
	BASE64: {
		encrypt(data) {
			return CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(data))
		},
		decrypt(cipher) {
			return CryptoJS.enc.Base64.parse(cipher).toString(CryptoJS.enc.Utf8)
		}
	},
	//AES加解密
	AES: {
		encrypt(data, secretKey, config = {}) {
			if (secretKey.length % 8 != 0) {
				console.warn("秘钥长度需为8的倍数，否则解密将会失败！")
			}
			const result = CryptoJS.AES.encrypt(data, CryptoJS.enc.Utf8.parse(secretKey), {
				iv: CryptoJS.enc.Utf8.parse(config.iv || ""),
				mode: CryptoJS.mode[config.mode || "ECB"],
				padding: CryptoJS.pad[config.padding || "Pkcs7"]
			})
			return result.toString()
		},
		decrypt(cipher, secretKey, config = {}) {
			const result = CryptoJS.AES.decrypt(cipher, CryptoJS.enc.Utf8.parse(secretKey), {
				iv: CryptoJS.enc.Utf8.parse(config.iv || ""),
				mode: CryptoJS.mode[config.mode || "ECB"],
				padding: CryptoJS.pad[config.padding || "Pkcs7"]
			})
			return CryptoJS.enc.Utf8.stringify(result);
		}
	}
}

export default tool
