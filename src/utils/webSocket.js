/** WebSocket 连接工具方法 */

// WebSocket 状态集合
const socketMap = new Map();

// 创建websocket
let creatWebSocket = (wsUrl, onNext) => {
	// 判断当前浏览器是否支持WebSocket
	if ("WebSocket" in window) {
		console.log('当前浏览器支持-WebSocket');
	} else if ("MozWebSocket" in window) {
		console.log('当前浏览器支持-MozWebSocket');
	} else {
		console.log('当前浏览器不支持-WebSocket');
	}

	try {
		initWebSocket(wsUrl, onNext);
	} catch (e) {
		console.log(`连接异常：${wsUrl}，尝试重新连接...`);
		reConnect(wsUrl);
	}
}

// 初始化websocket
let initWebSocket = (wsUrl, onNext) => {
	// 获取集合对象
	let instance = socketMap.get(wsUrl);

	if (instance) {
		instance.reconnect && clearTimeout(instance.reconnect);

		// 置空回调并关闭连接
		instance.socket.onopen = null;
		instance.socket.onmessage = null;
		instance.socket.onerror = null;
		instance.socket.onclose = null;
		instance.socket.close();

		instance.socket = new WebSocket(wsUrl);
	} else {
		// 创建ws实例，建立连接
		let webSocket = new WebSocket(wsUrl);

		// 保存实例对象
		socketMap.set(
			wsUrl,
			{
				socket: webSocket, //socket实例
				connect: false,    //是否连接
				reconnect: null,   //断线重连方法
				callback: onNext   //回调方法
			}
		)
	}

	// 建立连接
	socketMap.get(wsUrl).socket.onopen = (event) => {
		console.log(`连接成功：${wsUrl}`, event);

		let obj = socketMap.get(wsUrl);

		if (obj) {
			obj.connect = true;

			if (obj.callback) obj.callback('connect');
		}
	};

	// 接收数据
	socketMap.get(wsUrl).socket.onmessage = (event) => {
		console.log(`接收数据：${wsUrl}`, event);

		let obj = socketMap.get(wsUrl);

		if (obj && obj.callback) {
			obj.callback('receive', event.data);
		}
	};

	// 连接错误
	socketMap.get(wsUrl).socket.onerror = (event) => {
		console.log(`连接错误：${wsUrl}`, event);

		let obj = socketMap.get(wsUrl);

		if (obj)  {
			obj.connect = false;

			if (obj.callback) obj.callback('error');

			reConnect(wsUrl);
		}
	};

	// 关闭连接
	socketMap.get(wsUrl).socket.onclose = (event) => {
		console.log(`关闭连接：${wsUrl}`, event);

		let obj = socketMap.get(wsUrl);

		if (obj)  {
			obj.connect = false;

			if (obj.callback) obj.callback('close');

			// 非主动关闭时，触发重连机制
			reConnect(wsUrl);
		}
	};
}

// 重新连接
let reConnect = (wsUrl) => {
	let obj = socketMap.get(wsUrl);

	if (obj && !obj.connect) {
		obj.reconnect && clearTimeout(obj.reconnect);
		obj.reconnect = setTimeout(() => {
			// 延迟5秒重连，避免过多次过频繁请求重连
			creatWebSocket(wsUrl);
		}, 5000);
	}
};

// 发送数据
let sendWebSocket = (wsUrl, data) => {
	let sendData = data instanceof Object ? JSON.stringify(data) : data;

	console.log(`发送数据：${wsUrl}`, sendData);

	let obj = socketMap.get(wsUrl);

	if (obj && obj.socket && obj.connect) {
		obj.socket.send(sendData);
	} else {
		console.log(`未连接：${wsUrl}`)
	}
}

// 关闭连接
let closeWebSocket = (wsUrl) => {
	console.log(`主动关闭连接：${wsUrl}`);

	let obj = socketMap.get(wsUrl);

	if (obj && obj.socket) {
		obj.connect = false;
		obj.callback = null;
		obj.reconnect && clearTimeout(obj.reconnect);

		// 置空回调并关闭连接
		obj.socket.onopen = null;
		obj.socket.onmessage = null;
		obj.socket.onerror = null;
		obj.socket.onclose = null;
		obj.socket.close();

		if (obj.callback) obj.callback('close');
	}

	// 删除集合
	socketMap.delete(wsUrl);
};

export {
	socketMap,
	creatWebSocket,
	sendWebSocket,
	closeWebSocket
};
