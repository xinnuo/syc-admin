const {defineConfig} = require('@vue/cli-service')

module.exports = defineConfig({
	// 设置为空打包后不分根目录还是多级目录
	publicPath: './',

	// build编译后存放静态文件的目录
	//assetsDir: "static",

	// build编译后不生成资源MAP文件
	productionSourceMap: false,

	// 开发服务, build后的生产模式还需nginx代理
	devServer: {
		allowedHosts: 'all',
		open: false, //运行后自动打开浏览器
		port: process.env.VUE_APP_PORT, //挂载端口
		// 配置服务器代理
		proxy: {
			'/api': {
				// 对应的代理地址
				target: process.env.VUE_APP_API_BASEURL,
				// 如果要代理 websockets，配置这个参数
				ws: true,
				// 接受运行在https上，默认不接受
				secure: true,
				// 如果设置为true，那么本地会虚拟一个服务器接收你的请求并代你发送该请求，这样就不会有跨域问题（只适合开发环境）
				changeOrigin: false,
				// 重写路径 比如'/api/aaa/ccc'重写为'/aaa/ccc'
				pathRewrite: {
					'^/api': '/'
				}
			},
			// 配置多个代理
			'/sbl.nonggongchang.cn/ngc': {
				target: 'https://sbl.nonggongchang.cn/ngc',
				secure: true,
				pathRewrite: {
					'^/sbl.nonggongchang.cn/ngc': '',
				},
			},
			'/192.168.110.225:8081': {
				target: 'http://192.168.110.225:8081',
				secure: true,
				pathRewrite: {
					'^/192.168.110.225:8081': '',
				},
			},
			'/192.168.110.58:8081': {
				target: 'http://192.168.110.58:8081',
				secure: true,
				pathRewrite: {
					'^/192.168.110.58:8081': '',
				},
			},
		}
	},

	chainWebpack: config => {
		// 移除 prefetch 插件
		config.plugins.delete('preload');
		config.plugins.delete('prefetch');
		config.resolve.alias.set('vue-i18n', 'vue-i18n/dist/vue-i18n.cjs.js');
	},

	configureWebpack: {
		//性能提示
		performance: {
			hints: false
		},
		optimization: {
			splitChunks: {
				chunks: "all",
				automaticNameDelimiter: '~',
				name: "scuiChunks",
				cacheGroups: {
					//第三方库抽离
					vendor: {
						name: "modules",
						test: /[\\/]node_modules[\\/]/,
						priority: -10
					},
					elicons: {
						name: "elicons",
						test: /[\\/]node_modules[\\/]@element-plus[\\/]icons-vue[\\/]/
					},
					tinymce: {
						name: "tinymce",
						test: /[\\/]node_modules[\\/]tinymce[\\/]/
					},
					echarts: {
						name: "echarts",
						test: /[\\/]node_modules[\\/]echarts[\\/]/
					},
					xgplayer: {
						name: "xgplayer",
						test: /[\\/]node_modules[\\/]xgplayer.*[\\/]/
					},
					codemirror: {
						name: "codemirror",
						test: /[\\/]node_modules[\\/]codemirror[\\/]/
					}
				}
			}
		}
	}
})
